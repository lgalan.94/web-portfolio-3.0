import { AppNavbar, Layout, Loading, Transition, AnimatedText, Footer } from '../components';
import { useState, useEffect } from 'react';
import { motion } from 'framer-motion';
import { Link } from 'react-router-dom';
import { BsArrowUpRightSquare } from "react-icons/bs";

const Home = () => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 3500);
  }, []);

  return (
    <>
    <div className="bg-[#E6EBE7]" >
      {isLoading ? (
        <Loading />
      ) : (
        <motion.div
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ duration: 0.5 }}
          className=""
        >
          <AppNavbar />
          <Layout className="pt-0">
            <div className="flex items-center justify-between w-full"> 
              <div className="w-1/2">
                <motion.img src="pf2.png" 
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}
                  exit={{ opacity: 0 }}
                  transition={{ duration: 1 }}
                />
              </div>
              <div className="w-1/2 flex flex-col self-center">
                <AnimatedText text={`Hi, I am Lito Masbate Galan Jr`} className="!text-6xl text-left" />
                <motion.p
                className="tracking-wider pb-10 text-lg"
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}
                  exit={{ opacity: 0 }}
                  transition={{ duration: 1 }}
                >
                  {`Full-stack Web Developer from the Philippines`.split('').map((letter, index) => (
                    <motion.span
                      key={index}
                      initial={{ opacity: 0, x: -20 }}
                      animate={{ opacity: 1, x: 0 }}
                      transition={{ duration: 0.5, delay: index * 0.1 }}
                    >
                      {letter}
                    </motion.span>
                  ))}
                </motion.p>
                <div className="flex items-center self-start mt-2">
                  <Link 
                    as={Link} to='/' 
                    className="flex items-center bg-black text-white p-2 px-6 rounded-lg text-lg font-medium hover:bg-neutral-50 hover:text-neutral-700 border-2 border-solid border-transparent hover:border-neutral-800"
                  >Resume <BsArrowUpRightSquare className="w-6 ml-1" /> </Link>
                  <Link className="ml-4 text-lg font-medium capitalize text-neutral-800 underline">Contact</Link>
                </div>
              </div>
            </div>

            

          </Layout>
          <Footer />
        </motion.div>
      )}

    </div>
    
    </>
  );
};

export default Transition(Home); 