import Home from './Home';
import About from './About';
import Qualifications from './Qualifications';
import Contact from './Contact';
import PageNotFound from './PageNotFound';
import Projects from './Projects';
import Resume from './Resume';

export {
	Home,
	About,
	Qualifications,
	Contact,
	PageNotFound,
	Projects,
	Resume
}