import { AppNavbar, Transition, Layout, Footer, AnimatedText, Skills, Experience } from '../components';
import { motion } from 'framer-motion';

const Qualifications = () => {
  return (
    <div className="bg-defaultColor">
      <AppNavbar />
      <Layout className="pt-16" >
       <AnimatedText text="Skills" className="mb-16" />
         <Skills /> 
         <Experience />
      </Layout>

      <Footer />
    </div>
  );
};

export default Transition(Qualifications);