import { NavBar } from '../components';
import { motion } from 'framer-motion';

const Projects = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.5 }}
      className="justify-center"
    >
      <NavBar />
      <motion.h1
        initial={{ scale: 0 }}
        animate={{ scale: 1 }}
        transition={{ duration: 0.5 }}
        className="text-7xl text-center"
      >
        Projects Page
      </motion.h1>
    </motion.div>
  );
};

export default Projects;