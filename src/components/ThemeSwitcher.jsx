import { useState, useEffect } from 'react';
import { HiMoon, HiSun } from "react-icons/hi";

const ThemeSwitcher = () => {

const [theme, setTheme] = useState(null);

useEffect(() => {
	if (window.matchMedia("prefer-color-scheme: dark").matches) {
			setTheme('dark')
	} else {
			setTheme('light')
	}
}, [])

useEffect(() => {
	if (theme === 'dark') {
		document.documentElement.classList.add('dark')
	} else {
		document.documentElement.classList.remove('dark')
	}
},[theme])

const handleThemeSwitch = () => {
	setTheme(theme === 'dark' ? 'light' : 'dark')
}

	return (
		<button
		  onClick={handleThemeSwitch}
		  className="border p-2 rounded"
		>
		
		{theme === 'dark' ? <HiSun /> : <HiMoon />}

		</button>
	)
}

export default ThemeSwitcher