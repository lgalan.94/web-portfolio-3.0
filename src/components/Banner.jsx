import { motion, AnimatePresence } from 'framer-motion';
import { useState } from 'react';
import { FaYoutube, FaFacebookF, FaTwitter, FaGooglePlusG, FaLinkedinIn } from "react-icons/fa";

const Banner = () => {
  return (
    <div className="min-h-screen text-white dark:text-black bg-hero-pattern bg-no-repeat bg-cover dark:bg-neutral-700"> 
      <div className="bg-[#fc037b] bg-opacity-[20%] dark:bg-neutral-500 dark:bg-opacity-75 min-h-screen flex flex-wrap justify-center">
        <div className="banner w-full md:w-1/2 lg:w-1/2 self-center">
          <motion.div 
            className="flex mb-5 justify-center"
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{ duration: 1.3 }}
            >
            <a href="" target="_blank" className="p-1.5 hover:text-white hover:bg-red-500">
              <FaYoutube size={18} />
            </a>
            <a href="" target="_blank" className="p-1.5 hover:text-white hover:bg-blue-500">
              <FaFacebookF size={18} />
            </a>
            <a href="" target="_blank" className="p-1.5 hover:text-white hover:bg-[#3ebdb4]">
              <FaTwitter size={18} />
            </a>
            <a href="" target="_blank" className="p-1.5 hover:text-white hover:bg-[#e66a74]">
              <FaGooglePlusG size={18} />
            </a>
            <a href="" target="_blank" className="p-1.5 hover:text-white hover:bg-[#18b9d9]">
              <FaLinkedinIn size={18} />
            </a>
          </motion.div>
          <motion.h1
            className="text-7xl tracking-wide mb-3 overflow-hidden"
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{ duration: 1.3 }}
          >
            LITO GALAN JR
          </motion.h1>
          <AnimatePresence>
            <motion.p
            className="tracking-wider text-2xl"
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              transition={{ duration: 1 }}
            >
              {`Full-stack Web Developer from the Philippines`.split('').map((letter, index) => (
                <motion.span
                  key={index}
                  initial={{ opacity: 0, x: -20 }}
                  animate={{ opacity: 1, x: 0 }}
                  transition={{ duration: 0.5, delay: index * 0.1 }}
                >
                  {letter}
                </motion.span>
              ))}
            </motion.p>
          </AnimatePresence>

          

          {/*<p className="text-sm mt-5 font-thin">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>*/}
        </div>
      </div>
    </div>
  );
};

export default Banner;