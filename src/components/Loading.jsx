const Loading = () => {
  return (
    <div className="flex items-center justify-center h-screen">
      <svg className="animate-spin h-40 w-40" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
        <circle className="circle" cx="50" cy="50" r="45" />
      </svg>
    </div>
  );
};

export default Loading;