 import { Link, useLocation } from 'react-router-dom';
 import Logo from './Logo';

 const CustomLink = ({ endPoint, title, className = "", location }) => {
   const currentPath = useLocation().pathname;

   return (
     <Link as={Link} to={endPoint} className={`${className} relative group hover:text-neutral-600  ${currentPath === endPoint ? 'text-black' : 'text-neutral-400'}`} >
       {title}

       <span className={`h-[3px] inline-block w-0 bg-black opacity-50 absolute left-0 -bottom-0.5 group-hover:w-full transition-[width] ease duration-300 ${currentPath === endPoint ? 'w-full' : 'w-0'}`}>
         &nbsp;
       </span>
     </Link>
   );
 };

 const AppNavbar = () => {
   const location = useLocation();

   return (
     <header className="w-full px-32 py-8 font-medium flex items-center justify-between">
       <nav className="ms-auto">
         <CustomLink endPoint="/" title="Home" className="mr-4" location={location} />
         <CustomLink endPoint="/about" title="About" className="mx-4" location={location} />
         <CustomLink endPoint="/qualifications" title="Qualifications" className="mx-4" location={location} />
         <CustomLink endPoint="/contact" title="Contact" className="ml-4" location={location} />
       </nav>
       <Link as={Link} to='/' className="absolute left-3 top-5">
           <h2 className=""><Logo /></h2>
       </Link>
     </header>
   );
 };

 export default AppNavbar;