import { useLocation, Routes, Route, Navigate } from 'react-router-dom';
import './App.css';
import { Home, About, Qualifications, Contact, Projects, Resume, PageNotFound } from './pages';
import { AnimatePresence } from 'framer-motion';

function App() {

  const Inaccessible = () => {
    return < Navigate to = '/not-found' />
  }

  const location = useLocation();

  return (
   <AnimatePresence mode="wait">
    <Routes location={location} key={location.pathname} >
      <Route index element={<Home />} />
      <Route path='/about' element={<About />} />
      <Route path='/qualifications' element={<Qualifications />} />
      <Route path='/contact' element={<Contact />} />
      <Route path='/projects' element={<Projects />} />
      <Route path='/resume' element={<Resume />} />
      <Route path='/not-found' element={<PageNotFound />} />
      <Route path='*' element={<Inaccessible />} />
    </Routes>
   </AnimatePresence>
  );
}

export default App;