/** @type {import('tailwindcss').Config} */
module.exports = {
content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
    "./node_modules/tw-elements-react/dist/js/**/*.js"
],
theme: {
    extend: {
        backgroundImage: {
            circularLight: 'repeating-radial-gradient(rgba(0,0,0,0.4) 2px,#E6EBE7 5px,#E6EBE7 100px);',
        },
        colors:{
            defaultColor: '#E6EBE7',
        }
    },
},
darkMode: "class",
plugins: [require("tw-elements-react/dist/plugin.cjs")]
}